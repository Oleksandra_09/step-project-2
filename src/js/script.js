const navigationIcon = document.querySelector(".material-symbols-outlined");
        const navigationList = document.querySelector(".navigation__list");
        const navigationListLarge = document.querySelector(".navigation__listLarge");
        const navigationName = document.querySelector(".navigation__name");
navigationIcon.addEventListener("click", function(){
    if(navigationIcon.innerText === "menu"){
        navigationIcon.innerText = "close";
    }else {
        navigationIcon.innerText = "menu"
    }
    navigationList.classList.toggle("invisible");
})

if(window.matchMedia("(min-width: 481px)").matches){
    navigationList.classList.remove("invisible");
}
if(window.matchMedia("(min-width: 320px)").matches){
    navigationName.classList.add("invisible");
}
if(window.matchMedia("(min-width: 993px)").matches){
    navigationListLarge.classList.remove("invisible");
}
if(window.matchMedia("(min-width: 993px)").matches){
    navigationList.classList.add("invisible");
}
